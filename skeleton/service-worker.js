let APP_VERSION="1.0";
let ROOT_CACHE="offlinewebsite-"+APP_VERSION;
let CONTENT_CACHE="offlinewebsite-content";

// with trailing slash !
let ROOT_URL="/pwa/";
let CONTENT_URL=ROOT_URL+"c/libre/";
let FILELIST="filelist.txt"

let filesToCache = [
	ROOT_URL,
	ROOT_URL+"index.html",
	ROOT_URL+"js/main.js",
	ROOT_URL+"css/main.css"
];

self.addEventListener("install", function(e) {
	e.waitUntil(caches.open(ROOT_CACHE).then((cache) =>{
		return cache.addAll(filesToCache);
	}));
    console.log("service worker installed...");
});


// only return a response when the file exists in the cache
const getResponseFromCache = (cacheName, request) => {
	return caches.open(cacheName)
		.then(cache => {
			return cache.match(request);
		});
};

// store a request / response pair in the cache
const setResponseCache = (cachename, request, response) => {
	return caches.open(cacheName)
		.then(cache => {
			return cache.put(request, response);
		});
};

// cache first policy : return from cache if possible, from network if not (and make sure to fill the cache)
const getResponseFromCacheFirst = (cacheName, request) => {
	// retrieve in cache
	const response = getResponseFromCache(cacheName, request)
		.then((response) => {
			if (response) {
				// return response from cache if found
				console.log("cache hit: "+request.url);
				return response;
			} else {
				// request if not found
				console.log("cache miss: "+request.url);
				return fetch(request)
					.then(response => {
						if (response.ok) {
							// clone in cache when request succeded
							setResponseCache(cacheName, request, response.clone());
						} else {
							console.log("cache miss + fetch error: "+request.url);
							return null;
						}

						// return response for usage
						return response;
					});
			}
		});

	return response;
};

/*
// network first policy : return from network if possible (while updating the cache), from cache if not
const getResponseFromNetFirst = (CONTENT_CACHE, request) => {
	// request the file first
	return fetch(request)
		.then(response => {
			if (response.ok) {
				// clone in cache when request succeded
				setResponseCache(cacheName, request, response.clone());

				// return response for usage
				return response;
			} else {

			}
		});
	return response;
};
*/

self.addEventListener("fetch", event => {
	const requestUrl = new URL(event.request.url);

	// dont mix up data with app code
	if (requestUrl.pathname.includes(CONTENT_URL)) {
		cacheName=CONTENT_CACHE;
	} else {
		cacheName=ROOT_CACHE;
	}
	event.respondWith(getResponseFromCacheFirst(cacheName, event.request));
});

// get the filelist, a list of every file the website content
function loadfilelist() {
	return fetch(CONTENT_URL+FILELIST)
		.then(response => {
			if (response && response.ok) {
				response.text().then(text => {
					precache(text);
				});
			} else {
				console.log("fetch for filelist not working, will have to use cache");
			}
		})
}
// put in cache everything from the filelist
// text is the raw filelist.txt content
function precache(text) {
	linearray=text.split('\n');
	caches.open(CONTENT_CACHE).then(cache => {
		// each line contains a filename
		for (const line of linearray) {
			[checksum, filename]=line.split('*', 2);
			if (filename && filename.startsWith('./')) {
				filename=filename.substring(2);
			}
			console.log('low priority cache fill with: '+filename);
			cache.add(new Request(CONTENT_URL+filename, {importance: "low"}));
		}
	});
}

// called when the website is initialized (not on soft refresh, neither hard refresh because they ignore workers?)
self.addEventListener('activate', function(e) {
	console.log('service worker: Activate');
	loadfilelist();
	return self.clients.claim();
});
