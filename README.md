# PWA for website caching proof of concept
npm is not necessary if you have a webserver (host skeleton directory directly). HTTP2 recommended for higher performance.

Put some content in c/libre

Build the filelist in this directory with :
```
find -type f ! -name filelist.txt -exec sha256sum -b "{}" + > filelist.txt
```

Display the page. The website content will show up in an iframe. It will continue to work offline after a brief caching time. Cache operations show up in console.log

A lot needs to be improved (use checksums for cache key instead of filename, download indicator, multiple content management...)


Initially forked from : https://github.com/arjunmahishi/vanilla-pwa

